FROM python:3.10

WORKDIR /app

COPY static static/
COPY templates templates/
COPY app.py requirements.txt ./

RUN python3 -m pip install -r requirements.txt

CMD ["python3", "app.py"]
